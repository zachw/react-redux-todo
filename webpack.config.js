var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'webpack-hot-middleware/client',
    './index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: [ 'babel' ],
        exclude: /node_modules/,
        include: __dirname
      },
      {test: /\.css/, loader: 'style-loader!css-loader'},
      {test: /\.less$/, loader:  'style!css!less'},
      {
        test: /\.(png|jpg|gif|eot|ttf|svg|woff|woff2)$/,
        loader: 'url-loader?limit=8192'
      }
    ]
  }
}

import React, { Component, PropTypes } from 'react'

const uncheckedClass = 'glyphicon glyphicon-unchecked';
const checkedClass = 'glyphicon glyphicon-check completed';

class Todo extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      editing: false,
      text: this.props.todo.text || ''
    }
  }

  handleEditClick() {
    this.setState({ editing: true });
  }

  handleChange(e) {
    this.setState({ text: e.target.value });
  }

  handleSave(id, e) {
    e.preventDefault();
    const text = this.todoTextInput.value.trim();
    if (text.length === 0) {
      this.props.deleteTodo(id);
    } else {
      this.props.editTodo(id, text);
    }
    this.setState({
      editing: false,
      text: text
    })
  }

  formSubmit(id) {
    const text = this.todoTextInput.value.trim();
    if (text.length === 0) {
      this.props.deleteTodo(id);
    } else {
      this.props.editTodo(id, text);
    }
    this.setState({
      editing: false,
      text: text
    })
  };

  render() {
    const { todo, completeTodo, deleteTodo } = this.props;

    let textInputElement;
    if (this.state.editing) {
      textInputElement = (
        <form className="form-inline" onSubmit={this.handleSave.bind(this, todo.id)}>
          <input className="form-control input-lg"
                 placeholder={this.state.text}
                 value={this.state.text}
                 onChange={this.handleChange.bind(this)}
                 ref={(ref) => {this.todoTextInput = ref }} />
        </form>
      );
    } else {
      textInputElement = (
        <label>
          {todo.text}
        </label>
      );
    }

    let editSaveButtonElement;
    if (this.state.editing) {
      editSaveButtonElement = (
        <button className="btn btn-link todo-update" onClick={this.formSubmit.bind(this, todo.id)}>
          <i className="glyphicon glyphicon-save"></i>
        </button>
      )
    } else {
      editSaveButtonElement = (
        <button className="btn btn-link todo-edit" onClick={this.handleEditClick.bind(this)}>
          <i className="glyphicon glyphicon-edit"></i>
        </button>
      );
    }

    return (
      <tr className={(todo.completed) ? 'success' : ''}>
        <td className="todo-status-cell">
          <button className="btn btn-link" onClick={() => completeTodo(todo.id)}>
            <i className={todo.completed ? checkedClass : uncheckedClass}></i>
          </button>
        </td>
        <td>
          {textInputElement}
        </td>
        <td className="todo-edit-cell">
          {editSaveButtonElement}
        </td>
        <td className="todo-delete-cell">
          <button className="btn btn-link" onClick={() => deleteTodo(todo.id)}>
            <i className="glyphicon glyphicon-remove"></i>
          </button>
        </td>
      </tr>
    )
  }
}

Todo.propTypes = {
  todo: PropTypes.object.isRequired,
  editTodo: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  completeTodo: PropTypes.func.isRequired
};

export default Todo

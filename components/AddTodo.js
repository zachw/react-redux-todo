import React, { PropTypes, Component } from 'react'

class AddTodo extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      text: ''
    }
  }

  handleChange(e) {
    this.setState({ text: e.target.value })
  }

  handleSubmit(e) {
    e.preventDefault();
    const text = this.todoTextInput.value.trim();
    if (text.length !== 0) {
      this.props.addTodo(text);
    }
    this.setState({ text: '' });
  }

  formSubmit() {
    const text = this.todoTextInput.value.trim();
    if (text.length !== 0) {
      this.props.addTodo(text);
    }
    this.setState({ text: '' });
  };

  render() {
    return (
      <div className="input-group input-group-lg">
        <form className="form-inline" onSubmit={this.handleSubmit.bind(this)}>
          <input className="form-control input-lg"
                 placeholder="What to do?"
                 value={this.state.text}
                 onChange={this.handleChange.bind(this)}
                 ref={(ref) => {this.todoTextInput = ref }} />
        </form>
      <span className="input-group-btn">
        <button className="btn btn-primary btn-lg" onClick={this.formSubmit.bind(this)}>
          <i className="glyphicon glyphicon-plus"></i>
        </button>
      </span>
      </div>
    )
  }
}

AddTodo.propTypes = {
  addTodo: PropTypes.func.isRequired
};

export default AddTodo

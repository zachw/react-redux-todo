Todo List
=======================

The Todo List in this repository demonstrates how to use React/Redux.

Getting Started
---------------

Clone the repo:

```shell
$ git clone https://github.com/zachw/react-redux-todo.git
```

Install necessary dependencies:

```shell
$ npm install
```

Start the server:

```shell
$ npm start
```

Walkthrough
-----------

The actions folder contains a file called index.js.  This file just lists each of the actions that we want to use to
modify the state and their format.

Action example:

```
export const addTodo = (text) => {
  return {
    type: types.ADD_TODO,
    text
  }
};
```

In this example, we export the function addTodo.  We pass the text as a parameter to the addTodo function.  We then
return an action, which contains the action type, and the text that we want to add.

The components folder contain each of the React components we want to use in our Todo list webapp.  Notice that some of
these components use their own state management.  An example is the AddTodo component, which uses state to manage the
text that is being entered into the form.  The AddTodo component returns the fields required to add a todo item.  We
export the AddTodo component, so that it can be used in other components just by using the <AddTodo/> tag.

In the Todo component, there are method calls to dispatch the action.  Notice that these are being called in the same
way they are being declared in the actions file.  These actions then get sent to the reducers, where the actual logic of
manipulating the state occurs.  In this case, it will create a new object for the new todo, then create a new state
object (because the state is immutable) and add the new todo item to the end of the current list of todos.
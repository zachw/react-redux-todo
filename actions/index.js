import * as types from '../constants/ActionTypes'

export const addTodo = (text) => {
  return {
    type: types.ADD_TODO,
    text
  }
};

export const completeTodo = (id) => {
  return {
    type: types.COMPLETE_TODO,
    id
  }
};

export const deleteTodo = (id) => {
  return {
    type: types.DELETE_TODO,
    id
  }
};

export const editTodo = (id, text) => {
    return {
      type: types.EDIT_TODO,
      id,
      text
    }
};
